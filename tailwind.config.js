// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    darkMode: 'class',
    theme: {
        screens: {
            ...defaultTheme.screens,
        },
        extend: {
            colors: {
                primarydark: 'rgb(31 41 55)',
                btnlight: 'rgba(115, 160, 175, 0.3)',
            },
        },
    },
    plugins: [],
};

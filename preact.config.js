import Dotenv from 'dotenv-webpack';

export default function (config, env) {
    config.resolve.alias.src = env.src;
    config.resolve.modules.push(env.src);

    config.devServer.proxy = [
        {
            // proxy requests matching a pattern:
            path: '/api/**',
            // where to proxy to:
            target: 'http://localhost:8091',
            pathRewrite(path, request) {
                // you can modify the outbound proxy request here:
                delete request.headers.referer;
                // common: remove first path segment: (/api/**)
                return `/${path.replace(/^\/[^\/]+\//, '')}`;
            },
            // withCredentials: true,
            // optionally change Origin: and Host: headers to match target:
            changeOrigin: true,
            changeHost: true,
        },
        {
            // proxy requests matching a pattern:
            path: '/socket.io',
            // where to proxy to:
            target: 'ws://localhost:8091',

            // withCredentials: true,
            // optionally change Origin: and Host: headers to match target:
            // changeOrigin: true,
            changeHost: true,
            ws: true,
        },
    ];
    config.plugins.push(new Dotenv());
}

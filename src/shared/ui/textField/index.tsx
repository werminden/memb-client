import { h, JSX } from 'preact';
import cn from 'classnames';
import { forwardRef } from 'preact/compat';
import styles from './styles.module.css';

const TextField = forwardRef<HTMLInputElement, JSX.HTMLAttributes>(
    ({ className, ...rest }, ref) => {
        return (
            <div className={cn(styles.field, className)}>
                <div className="flex-grow">
                    <input className={styles.input} {...rest} ref={ref} />
                </div>
            </div>
        );
    },
);

export default TextField;

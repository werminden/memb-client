import { cloneElement, Fragment, h, JSX, VNode } from 'preact';
import cn from 'classnames';
import { useState } from 'preact/hooks';
import { forwardRef } from 'preact/compat';
import { JSXInternal } from 'preact/src/jsx';
import { callOptionalFunc } from 'shared/lib/helper';
import styles from './styles.module.css';

const DEFAULT_CLICK_POSITION = { x: 0, y: 0 };

type Props = {
    component?: VNode;
    showFocus?: boolean;
    disableSoftly?: boolean;
    active?: boolean;
};

const BaseBtn = forwardRef<HTMLButtonElement, JSX.HTMLAttributes & Props>(
    (
        {
            children,
            showFocus,
            onMouseDown,
            onMouseUp,
            onMouseLeave,
            onFocus,
            onBlur,
            onClick,
            component,
            ...restProps
        },
        ref,
    ) => {
        const [rippled, setRipple] = useState(false);
        const [clickPosition, setClickPosition] = useState(DEFAULT_CLICK_POSITION);
        const [focused, setFocused] = useState(false);

        const handleFocus: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            setFocused(true);
            callOptionalFunc(onFocus, event);
        };

        const handleBlur: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            setFocused(false);
            callOptionalFunc(onBlur, event);
        };

        const handleMouseDown: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            setClickPosition({ x: event.offsetX, y: event.offsetY });
            setRipple(true);
            callOptionalFunc(onMouseDown, event);
        };

        const handleMouseUp: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            setRipple(false);
            callOptionalFunc(onMouseUp, event);
        };

        const handleMouseLeave: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            setRipple(false);
            callOptionalFunc(onMouseLeave, event);
        };

        const handleMouseClick: JSXInternal.MouseEventHandler<EventTarget> = (event) => {
            if (!restProps.disableSoftly) {
                callOptionalFunc(onClick, event);
            }
        };

        const Component = cloneElement(
            component || <button type="button" />,
            {
                ...restProps,
                ref,
                className: cn(styles.root, restProps.className, {
                    [styles.disabled]: restProps.disabled,
                    [styles.focused]: focused && showFocus,
                    [styles.active]: !restProps.disabled && restProps.active,
                }),
                onMouseDown: handleMouseDown,
                onMouseUp: handleMouseUp,
                onMouseLeave: handleMouseLeave,
                onBlur: handleBlur,
                onFocus: handleFocus,
                onClick: handleMouseClick,
            },
            <Fragment>
                <div
                    className={styles.ripplebox}
                    style={{
                        top: `${clickPosition.y}px`,
                        left: `${clickPosition.x}px`,
                    }}
                >
                    <div
                        className={cn(styles.ripple, {
                            [styles['ripple__active']]: !restProps.disabled && rippled,
                        })}
                    />
                </div>
                {children}
            </Fragment>,
        );

        return Component;
    },
);

export default BaseBtn;

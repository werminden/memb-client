import { h, JSX } from 'preact';
import cn from 'classnames';
import { useEffect, useImperativeHandle, useMemo, useRef, useState } from 'preact/hooks';
import { forwardRef } from 'preact/compat';
import { SelectItem } from 'shared/types';
import BaseBtn from '../baseBtn';
import styles from './styles.module.css';

type Props = {
    options?: (SelectItem | string)[];
    defaultValues?: string[];
    onChange?: (values: string[]) => void;
};

const SelectField = forwardRef<HTMLInputElement, JSX.HTMLAttributes & Props>(
    ({ className, options = [], placeholder, defaultValues = [], onChange }, ref) => {
        const filterInputRef = useRef<HTMLInputElement>(null);
        useImperativeHandle(ref, () => filterInputRef.current!);

        const [cursor, setCursor] = useState(0);
        const [hovered, setHovered] = useState<string | undefined>(undefined);

        const [selected, setSelected] = useState<string[]>(defaultValues);
        const [filter, setFilter] = useState('');

        const safeOptions = useMemo(
            () =>
                options.reduce<Record<string, SelectItem>>((acc, option) => {
                    const isObject = typeof option === 'object';
                    const text = isObject ? option.text : option;
                    const value = isObject ? option.value : option;
                    return { ...acc, [value]: { text, value } };
                }, {}),
            [options],
        );

        const selectedOptions = useMemo(
            () => Object.values(safeOptions).filter((item) => !selected.includes(item.value)),
            [safeOptions, selected],
        );

        const filteredOptions = useMemo(
            () =>
                selectedOptions.filter(({ text, value }) => {
                    const filterValue = filter.toLowerCase();
                    const checkValue = `${text.toLowerCase()} ${value.toLowerCase()}`;
                    return checkValue.includes(filterValue);
                }),
            [selectedOptions, filter],
        );

        useEffect(() => {
            if (filteredOptions.length && hovered) {
                setCursor(filteredOptions.findIndex((option) => option.value === hovered));
            }
        }, [hovered, filteredOptions]);

        useEffect(() => {
            if (onChange) {
                onChange(selected);
            }
        }, [selected]);

        const removeSelected = (value: string) => {
            setSelected((prevState) => prevState.filter((selectedItem) => value !== selectedItem));
        };

        const addSelected = (value: string) => {
            setSelected((prevState) => Array.from(new Set([...prevState, value])));
            setFilter('');
            setTimeout(() => setCursor(0), 100);
        };

        const cleanSelected = () => {
            setSelected([]);
        };

        const onFilterChange: JSX.GenericEventHandler<HTMLInputElement> = (e) => {
            setFilter(e.currentTarget.value);
        };

        const selectedItems = useMemo(
            () =>
                selected.map((selectedItemValue) => {
                    const { value, text } = safeOptions[selectedItemValue];
                    return (
                        <div
                            key={`option-${value}`}
                            className="flex flex-row bg-slate-700 rounded-sm border-2 border-slate-400 text-base px-2"
                        >
                            <div className="mr-2">{text}</div>
                            <div role="button" onClick={() => removeSelected(value)}>
                                x
                            </div>
                        </div>
                    );
                }),
            [safeOptions, selected],
        );

        const avilableItems = useMemo(
            () =>
                filteredOptions.map(({ value, text }, index) => (
                    <BaseBtn
                        key={`option-${value}`}
                        onClick={() => addSelected(value)}
                        onMouseEnter={() => setHovered(value)}
                        onMouseLeave={() => setHovered(undefined)}
                        active={index === cursor}
                    >
                        <div className="flex justify-between flex-row items-center">
                            <div>{text}</div>
                            <BaseBtn>o</BaseBtn>
                        </div>
                    </BaseBtn>
                )),
            [filteredOptions, cursor],
        );

        const onFilterKeyDown: JSX.KeyboardEventHandler<HTMLInputElement> = (e) => {
            switch (e.key) {
                case 'Backspace': {
                    const lastSelectedItem = selected[selected.length - 1];
                    if (lastSelectedItem && !filter.length) {
                        removeSelected(lastSelectedItem);
                    }
                    break;
                }
                case 'ArrowUp': {
                    setCursor((prevState) =>
                        prevState > 0 ? prevState - 1 : filteredOptions.length - 1,
                    );
                    break;
                }
                case 'ArrowDown': {
                    setCursor((prevState) =>
                        prevState < filteredOptions.length - 1 ? prevState + 1 : 0,
                    );
                    break;
                }
                case 'Enter': {
                    const option = filteredOptions[cursor];
                    if (option) {
                        addSelected(option.value);
                    }
                    e.preventDefault();
                    break;
                }
            }
        };

        return (
            <div>
                <div className={cn(styles.field, className)}>
                    <div className="flex-grow">
                        <div className="flex flex-row">
                            <div className="flex-1 flex flex-row items-center flex-wrap">
                                {selectedItems}
                                <div className="flex-1">
                                    <input
                                        ref={filterInputRef}
                                        className={styles.input}
                                        value={filter}
                                        onChange={onFilterChange}
                                        onKeyDown={onFilterKeyDown}
                                        placeholder={placeholder}
                                    />
                                </div>
                            </div>
                            {!!selected.length && (
                                <div className="flex items-center">
                                    <BaseBtn
                                        type="button"
                                        className="h-full w-8"
                                        onClick={cleanSelected}
                                    >
                                        X
                                    </BaseBtn>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="max-h-[228px] overflow-y-auto mt-4 mr-[-12px]">
                    <div className="flex flex-col mr-3">{avilableItems}</div>
                </div>
            </div>
        );
    },
);

export default SelectField;

import { createContext } from 'preact';

export const DEFAULT_MENU_CONTEXT = { isOpen: false };

export type MenuContextValue = {
    onClick?: () => void;
    onClose?: () => void;
    isOpen: boolean;
};

export const MenuContext = createContext<MenuContextValue>(DEFAULT_MENU_CONTEXT);

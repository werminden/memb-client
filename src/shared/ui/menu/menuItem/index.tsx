import { FunctionComponent, h, JSX, VNode } from 'preact';
import { useContext } from 'preact/hooks';
import { BaseBtn } from 'shared/ui';
import { callOptionalFunc } from 'shared/lib/helper';
import { MenuContext } from '../context';

const MenuItem: FunctionComponent<JSX.HTMLAttributes & { component?: VNode }> = ({
    children,
    onClick,
    ...restProps
}) => {
    const menuCtx = useContext(MenuContext);

    const isRenderProps = typeof children === 'function';

    const handleClick: JSX.MouseEventHandler<EventTarget> = (event) => {
        callOptionalFunc(onClick, event);
        callOptionalFunc(menuCtx.onClick);
    };

    return (
        <BaseBtn
            {...restProps}
            onClick={!isRenderProps ? handleClick : undefined}
            className="text-sm !text-left w-full"
        >
            {isRenderProps ? children(menuCtx) : children}
        </BaseBtn>
    );
};

export default MenuItem;

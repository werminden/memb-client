import cn from 'classnames';
import { FunctionalComponent, h } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import { useWindowSizeContext, useOutsideAlerter } from 'shared/lib/hooks';
import { MenuContext } from './context';
import { Coord } from 'shared/types';

type Props = {
    isOpen: boolean;
    position?: Coord;
    anchorEl?: HTMLElement | null;
    className?: string;
    onClose?: () => void;
    onClick?: () => void;
};

const Menu: FunctionalComponent<Props> = ({
    className,
    children,
    isOpen,
    anchorEl,
    position,
    onClose,
    onClick,
}) => {
    const [rect, setRect] = useState<Partial<Coord>>({});
    const { windowSize } = useWindowSizeContext();
    const containerRef = useRef<HTMLDivElement>(null);
    useOutsideAlerter([containerRef.current!, anchorEl!], onClose);

    const correlateContainerPosition = (click: Coord) => {
        const containerWidth = containerRef.current?.clientWidth;
        const containerHight = containerRef.current?.clientHeight;
        const hasXLimit = click.x + Number(containerWidth) > Number(windowSize.width);
        const hasYLimit = click.y + Number(containerHight) > Number(windowSize.height);

        return {
            y: hasYLimit ? click.y - Number(containerHight) : click.y,
            x: hasXLimit ? click.x - Number(containerWidth) : click.x,
        };
    };

    useEffect(() => {
        if (anchorEl) {
            if (position) {
                setRect(correlateContainerPosition(position));
            } else {
                const rect = anchorEl.getBoundingClientRect();
                setRect({ x: rect.left, y: rect.bottom });
            }
        }
    }, [anchorEl, windowSize, position]);

    return (
        <MenuContext.Provider value={{ onClick, isOpen }}>
            <div
                ref={containerRef}
                className={cn(
                    'fixed mt-1 rounded-sm transition-opacity duration-200 ease-in origin-top opacity-0 scale-0 overflow-hidden z-50 light-shadow',
                    className,
                    {
                        'opacity-100 scale-100': isOpen,
                    },
                )}
                style={{
                    left: `${rect.x}px`,
                    top: `${rect.y}px`,
                }}
            >
                {isOpen ? children : null}
            </div>
        </MenuContext.Provider>
    );
};

export default Menu;

import { FunctionalComponent, h } from 'preact';
import { Link, LinkProps } from 'react-router-dom';
import cn from 'classnames';
import styles from './styles.module.css';

const NavLinkBtn: FunctionalComponent<LinkProps> = ({ to, children, className, ...props }) => {
    return (
        <Link to={to} {...props} className={cn(styles.link, 'glow-border', className)}>
            {children}
        </Link>
    );
};

export default NavLinkBtn;

import { FunctionalComponent, h } from 'preact';
import { Link, LinkProps } from 'react-router-dom';
import cn from 'classnames';

const NavLink: FunctionalComponent<LinkProps> = ({ to, children, className }) => {
    return (
        <Link
            to={to}
            className={cn('text-cyan-600 hover:text-cyan-500 hover:underline', className)}
        >
            {children}
        </Link>
    );
};

export default NavLink;

import { FunctionComponent, h } from 'preact';

const Loader: FunctionComponent = () => {
    return (
        <div className="w-screen h-screen absolute bg-red">
            <div className="bg-red rounded-full h-20 w-20" />
        </div>
    );
};

export default Loader;

import {
    BaseQueryFn,
    FetchArgs,
    fetchBaseQuery,
    FetchBaseQueryError,
} from '@reduxjs/toolkit/query';
import { Mutex } from 'async-mutex';
import { setToken, loggedOut } from 'store/slices/auth';
import { RootState } from 'store';

const BASE_URL = process.env.BASE_URL;
const API_URL = `${BASE_URL}/api`;

// create a new mutex
const mutex = new Mutex();

export const refreshQuery = fetchBaseQuery({
    baseUrl: API_URL,
});

const baseQuery = fetchBaseQuery({
    baseUrl: API_URL,
    prepareHeaders: (headers, { getState, endpoint }) => {
        const token = (getState() as RootState).auth.tkn;
        // If we have a token set in state, let's assume that we should be passing it.

        const isAuthRoute = ['token', 'signIn', 'signOut', 'signUp'].some((v) =>
            endpoint.includes(v),
        );

        if (token && !isAuthRoute) {
            headers.set('authorization', `Bearer ${token}`);
        }
        return headers;
    },
});

const baseQueryWithReauth: BaseQueryFn<string | FetchArgs, unknown, FetchBaseQueryError> = async (
    args,
    api,
    extraOptions,
) => {
    // wait until the mutex is available without locking it
    await mutex.waitForUnlock();
    let result = await baseQuery(args, api, extraOptions);
    if (result.error && result.error.status === 401) {
        // checking whether the mutex is locked
        if (!mutex.isLocked()) {
            const release = await mutex.acquire();
            try {
                const refreshResult = await refreshQuery(
                    { url: '/v1/auth/token', credentials: 'include' },
                    api,
                    extraOptions,
                );
                if (refreshResult.data) {
                    api.dispatch(setToken(refreshResult.data as any));
                    // retry the initial query
                    result = await baseQuery(args, api, extraOptions);
                } else {
                    api.dispatch(loggedOut());
                }
            } finally {
                // release must be called once the mutex should be released again.
                release();
            }
        } else {
            // wait until the mutex is available without locking it
            await mutex.waitForUnlock();
            result = await baseQuery(args, api, extraOptions);
        }
    }
    return result;
};

export default baseQueryWithReauth;

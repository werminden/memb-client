import { SelectItem } from 'shared/types';

export enum QuizStatus {
    init = 'INIT',
    story = 'STORY',
    activation = 'ACTIVATION',
    question = 'QUESTION',
    done = 'DONE',
}

export enum AnswerResult {
    passed = 1,
    failed = 0,
    partially = 2,
}

export type QuizState = {
    id: number;
    status: QuizStatus;
    gameSessionId: number;
    quiz: Quiz;
    quizId: number;
    storyMessageId: number;
    questionMessageId?: number;
    registeredAt?: number;
    activatedAt?: number;
    resolvedAt?: number;
    result?: AnswerResult;
};

export type Quiz = {
    id: number;
    author: Author;
    type: QuizType;
    variants?: SelectItem[] | string[];
    message: string;
    question: string;
    answer: string;
};

export enum QuizType {
    text = 'TEXT',
    select = 'SELECT',
}

export type Author = {
    username: string;
};

export type MessageData = {
    id: number;
    quizId: number;
    sender: Author;
    text: string;
    read: boolean;
    readAt?: Date;
    gameSessionId: number;
    params?: Record<string, string> | null;
    createdAt: Date;
};
export interface GameSession {
    id: number;
    owner: Author;
    note: string;
    types: QuizType[];
    states?: QuizState[];
    createdAt: string;
}

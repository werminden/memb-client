export * from './session';
export * from './user';

export type SelectItem = {
    text: string;
    value: string;
};

export type Coord = {
    x: number;
    y: number;
};

export interface GetManyResponse<T> {
    total: number;
    result: T[];
}

export enum Order {
    ASC = 'ASC',
    DESC = 'DESC',
}

export interface PagingQuery {
    afterCursor?: string;
    beforeCursor?: string;
    limit?: number;
    order?: Order | 'ASC' | 'DESC';
}

export interface Cursor {
    beforeCursor: string | null;
    afterCursor: string | null;
}

export interface PagingResult<Entity> {
    data: Entity[];
    cursor: Cursor;
}

export type IdParamPayload = {
    id: number;
};

export enum ResponseStatus {
    OK = 'OK',
    NOK = 'NOK',
}
export interface SocketResponse<T> {
    status: ResponseStatus;
    error?: Error;
    data?: T;
}

export const routes = {
    game: '/me',
    login: '/login',
    register: '/register',
    settings: '/settings',
    quiz: '/me/quiz',
} as const;

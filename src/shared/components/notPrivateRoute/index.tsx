import { FunctionComponent, h } from 'preact';
import { Loader } from 'shared/ui';
import { useAppSelector } from 'app/hooks/auth';
import { selectIsAuth } from 'store/slices/auth';
import { useGetMeQuery } from 'store/services/auth';
import { Navigate, Outlet, To } from 'react-router-dom';

type Props = {
    redirectTo?: To;
};

const NotPrivateRoute: FunctionComponent<Props> = ({ redirectTo = '/' }) => {
    const isAuth = useAppSelector(selectIsAuth);
    const { isLoading } = useGetMeQuery();

    if (isLoading) return <Loader />;
    return !isAuth ? <Outlet /> : <Navigate to={redirectTo} />;
};

export default NotPrivateRoute;

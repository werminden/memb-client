import { FunctionComponent, h } from 'preact';
import { Loader } from 'shared/ui';
import { useAppSelector } from 'app/hooks/auth';
import { selectIsAuth } from 'store/slices/auth';
import { useGetMeQuery } from 'store/services/auth';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { routes } from 'shared/constants';

const PrivateRoute: FunctionComponent = () => {
    const isAuth = useAppSelector(selectIsAuth);
    const { isLoading } = useGetMeQuery();
    const location = useLocation();

    if (isLoading) return <Loader />;

    return isAuth ? <Outlet /> : <Navigate to={routes.login} state={{ from: location }} />;
};

export default PrivateRoute;

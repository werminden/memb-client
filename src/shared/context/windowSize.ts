import { createContext } from 'preact';

export type WindowSize = {
    width?: number;
    height?: number;
};

export type WindowSizeData = {
    isMobile: boolean;
    windowSize: WindowSize;
};

export const DEFAULT_WINDOW_SIZE = {
    isMobile: false,
    windowSize: {
        width: undefined,
        height: undefined,
    },
};

export const WindowSizeContext = createContext<WindowSizeData>(DEFAULT_WINDOW_SIZE);

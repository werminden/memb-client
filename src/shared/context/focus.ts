import { createContext } from 'preact';

type FocusData = {
    currentFocus: Element | null;
    prevFocus: Element | null;
};

export const FocusContext = createContext<FocusData>({ currentFocus: null, prevFocus: null });

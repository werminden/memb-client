import { createContext } from 'preact';
import { useGameSession } from 'features/session/model';

export const GameContext = createContext({} as GameSessionCtx);

export type GameSessionCtx = ReturnType<typeof useGameSession>;

const TOKEN_KEY = 'mbtkn';

const saveAuthToken = (token: string) => sessionStorage.setItem(TOKEN_KEY, token);
const getAuthToken = () => sessionStorage.getItem(TOKEN_KEY);
const removeAuthToken = () => sessionStorage.removeItem(TOKEN_KEY);

export { TOKEN_KEY, saveAuthToken, getAuthToken, removeAuthToken };

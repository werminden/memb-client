import { RefObject } from 'preact';
import { useEffect, useState } from 'preact/hooks';

export const useFocusOnElement = (refEl: RefObject<HTMLInputElement | HTMLButtonElement>) => {
    const [focused, setFocused] = useState(false);

    function onBlur() {
        setFocused(false);
    }

    function onFocus() {
        setFocused(true);
    }

    useEffect(() => {
        const el = refEl.current;
        console.log(el);
        el?.addEventListener('blur', onBlur);
        el?.addEventListener('focus', onFocus);

        return () => {
            el?.removeEventListener('blur', onBlur);
            el?.removeEventListener('focus', onFocus);
        };
    }, []);

    return focused;
};

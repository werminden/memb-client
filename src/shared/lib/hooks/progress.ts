import { useCallback, useState } from 'preact/hooks';

export type ProgressWrap = <T, A>(cb: ReturnType<(params: A) => Promise<T>>) => Promise<T>;

export const useProgress = (defaulProgressState = false) => {
    const [inProgress, setInProgress] = useState(defaulProgressState);

    const wrap: ProgressWrap = useCallback(
        async (cb) => {
            setInProgress(true);

            try {
                const result = await cb;
                return result;
            } finally {
                setInProgress(false);
            }
        },
        [setInProgress],
    );

    return { inProgress, setInProgress, wrap };
};

import { useContext } from 'preact/hooks';
import { WindowSizeContext } from '../../context';

export const useWindowSizeContext = () => {
    const windowSize = useContext(WindowSizeContext);

    return windowSize;
};

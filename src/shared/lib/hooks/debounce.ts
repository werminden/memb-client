import { useRef } from 'preact/hooks';

type DebounceConf = {
    delay: number;
};

export const useDebounce = ({ delay }: DebounceConf) => {
    const timeout = useRef<any>();

    const debounce = <T>(action: () => T) => {
        clearTimeout(timeout.current);
        timeout.current = setTimeout(action, delay);
    };

    return debounce;
};

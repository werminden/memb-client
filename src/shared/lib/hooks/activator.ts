import { Coord } from 'shared/types';
import { useState } from 'preact/hooks';

export const useActivator = () => {
    const [anchorEl, setAnchor] = useState<HTMLElement | null>(null);
    const [clickPosition, setClickPosition] = useState<Coord>();
    const isOpen = Boolean(anchorEl);
    const open = (event: MouseEvent) => {
        setAnchor(event.currentTarget as HTMLElement);
        setClickPosition({ x: event.pageX, y: event.pageY });
    };

    const close = () => {
        setAnchor(null);
        setClickPosition(undefined);
    };

    return { isOpen, clickPosition, anchorEl, open, close };
};

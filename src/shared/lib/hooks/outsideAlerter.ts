import { useEffect } from 'preact/hooks';

export const useOutsideAlerter = (refs: (HTMLElement | null)[], cb?: () => void) => {
    useEffect(() => {
        function handleClickOutside(event: MouseEvent) {
            if (refs.every((ref) => ref && !ref.contains(event.target as Node)) && cb) {
                cb();
            }
        }

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [refs, cb]);
};

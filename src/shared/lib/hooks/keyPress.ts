import { useEffect, useState } from 'preact/hooks';

export const useKeyPress = (targetKey: string) => {
    const [keyPressed, setKeyPressed] = useState(false);

    function downHandler(this: Window, { key }: KeyboardEvent) {
        if (key === targetKey) {
            setKeyPressed(true);
        }
    }

    function upHandler(this: Window, { key }: KeyboardEvent) {
        if (key === targetKey) {
            setKeyPressed(false);
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', downHandler);
        window.addEventListener('keyup', upHandler);

        return () => {
            window.removeEventListener('keydown', downHandler);
            window.removeEventListener('keyup', upHandler);
        };
    });

    return keyPressed;
};

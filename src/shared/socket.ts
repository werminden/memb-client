import { io } from 'socket.io-client';
import { Socket } from 'socket.io-client/build/esm/socket';
import { loggedOut } from 'store/slices';
import { tokenApi } from 'store/services';

const BASE_URL = process.env.BASE_URL;

let socket: Socket;
export async function getSocket() {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { store } = await import('store').then((module) => module);
    const token = store.getState().auth.tkn;
    let reconnectionAttempts = 0;
    if (!socket) {
        socket = io(String(BASE_URL!).replace(/^http/, 'ws'), {
            transports: ['websocket'],
            auth: { token },
        });

        socket.on('connect_error', (err) => {
            if (String(err?.message).toLowerCase() === 'unauthorized') {
                const timeout = reconnectionAttempts === 0 ? 5000 : 60000;
                console.log(`Trying to reconnect in the next ${timeout / 1000} seconds`);
                setTimeout(async function () {
                    console.log('Trying to reconnect manually');
                    reconnectionAttempts++;
                    try {
                        const refreshResult = await store
                            .dispatch(tokenApi.endpoints.refresh.initiate())
                            .unwrap();
                        socket.auth = (data) => ({ ...data, token: refreshResult.tkn });
                        socket.connect();
                        return socket;
                    } catch (e) {
                        console.error(e);
                        store.dispatch(loggedOut());
                    }
                }, timeout);
            }
        });
    }
    return socket;
}

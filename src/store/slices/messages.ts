import { RootState } from 'store';
import { createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Cursor, MessageData } from 'shared/types';
import { gameApi } from 'store/services';

const messagesAdapter = createEntityAdapter<MessageData>();

export const messagesSlice = createSlice({
    name: 'messages',
    initialState: messagesAdapter.getInitialState<{ cursor: Cursor }>({
        cursor: { beforeCursor: null, afterCursor: null },
    }),
    reducers: {
        setMessages: (state, { payload }: PayloadAction<MessageData[]>) => {
            messagesAdapter.setMany(state, payload);
        },
    },
    extraReducers: (builder) => {
        builder.addMatcher(gameApi.endpoints.getMessages.matchFulfilled, (state, { payload }) => {
            messagesAdapter.setMany(state, payload.data || []);
            state.cursor = payload.cursor;
        });
    },
});

export const { setMessages } = messagesSlice.actions;

export const messagesSelectorBySessionId = (state: RootState, sessionId: number) =>
    Object.values(state.messages.entities).filter(
        (message) => message?.gameSessionId === sessionId,
    ) as MessageData[];

export const messagesCursorSelector = (state: RootState) => state.messages.cursor;

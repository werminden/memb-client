import { createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { QuizState } from 'shared/types';
import { gameApi } from '../services';

export const currentQuizStatesAdapter = createEntityAdapter<QuizState>({
    selectId: (entity) => entity.gameSessionId,
});

export const currentQuizStateSlice = createSlice({
    name: 'currentQuizState',
    initialState: currentQuizStatesAdapter.getInitialState(),
    reducers: {
        setCurrentQuizState: (state, { payload }: PayloadAction<QuizState>) => {
            currentQuizStatesAdapter.setOne(state, payload);
        },
        removeCurrentQuizState: (state, { payload }: PayloadAction<number>) => {
            currentQuizStatesAdapter.removeOne(state, payload);
        },
    },
    extraReducers(builder) {
        builder.addMatcher(
            gameApi.endpoints.subscribeOnCurrentQuizState.matchFulfilled,
            (
                state,
                {
                    payload,
                    meta: {
                        arg: { originalArgs },
                    },
                },
            ) => {
                if (payload) {
                    return currentQuizStatesAdapter.setOne(state, payload);
                }
                if (payload === null) {
                    currentQuizStatesAdapter.removeOne(state, originalArgs);
                }
            },
        );
    },
});

export const { setCurrentQuizState, removeCurrentQuizState } = currentQuizStateSlice.actions;

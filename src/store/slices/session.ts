import { RootState } from 'store';
import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { Cursor, GameSession } from 'shared/types';
import { sessionApi } from '../services';

const sessionsAdapter = createEntityAdapter<GameSession>({
    sortComparer: (a, b) => {
        return b.createdAt.localeCompare(a.createdAt);
    },
});

export const sessionSlice = createSlice({
    name: 'sessions',
    initialState: sessionsAdapter.getInitialState<{ cursor: Cursor }>({
        cursor: { beforeCursor: null, afterCursor: null },
    }),
    reducers: {},
    extraReducers: (builder) => {
        builder.addMatcher(
            sessionApi.endpoints.getSessions.matchFulfilled,
            (state, { payload }) => {
                sessionsAdapter.setMany(state, payload.data);
                state.cursor = payload.cursor;
            },
        );
        builder.addMatcher(
            sessionApi.endpoints.createSession.matchFulfilled,
            (state, { payload }) => {
                sessionsAdapter.addOne(state, payload);
            },
        );
        builder.addMatcher(
            sessionApi.endpoints.deleteSession.matchFulfilled,
            (state, { meta: { arg } }) => {
                sessionsAdapter.removeOne(state, arg.originalArgs.id);
            },
        );
    },
});

export const sessionSelector = sessionsAdapter.getSelectors<RootState>((state) => state.sessions);
export const sessionCursorSelector = (state: RootState) => state.sessions.cursor;

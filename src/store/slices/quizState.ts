import { RootState } from 'store';
import { createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { QuizState, Cursor } from 'shared/types';
import { sessionApi } from 'store/services';

export const quizStatesAdapter = createEntityAdapter<QuizState>();

export const quizStateSlice = createSlice({
    name: 'quizStates',
    initialState: quizStatesAdapter.getInitialState<{ cursor: Cursor }>({
        cursor: { beforeCursor: null, afterCursor: null },
    }),
    reducers: {
        setQuizState: (state, { payload }: PayloadAction<QuizState>) => {
            quizStatesAdapter.setOne(state, payload);
        },
    },
    extraReducers: (builder) => {
        builder.addMatcher(
            sessionApi.endpoints.getSessionState.matchFulfilled,
            (state, { payload }) => {
                quizStatesAdapter.setMany(state, payload.data || []);
            },
        );
    },
});

export const { setQuizState } = quizStateSlice.actions;

export const quizStatesSelectorBySession = (state: RootState, sessionId: number) =>
    Object.values(state.quizState.entities).filter(
        (qs) => qs?.gameSessionId === sessionId,
    ) as QuizState[];

export const quizStatesEntityByQuizForSessionSelector = (state: RootState, sessionId: number) =>
    Object.values(state.quizState.entities).reduce<Record<number, QuizState>>(
        (acc, qs) => (qs?.gameSessionId === sessionId ? { ...acc, [qs!.quizId]: qs! } : acc),
        {},
    );

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { authApi, tokenApi } from 'store/services';
import { User } from 'shared/types';

export type AuthState = {
    tkn: string | null;
    user: User | null;
};

const initialState: AuthState = {
    tkn: null,
    user: null,
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setToken: (state, { payload: { tkn } }: PayloadAction<{ tkn: string }>) => {
            state.tkn = tkn;
        },
        loggedOut: (state) => {
            state.tkn = null;
            state.user = null;
        },
    },
    extraReducers: (builder) => {
        builder.addMatcher(authApi.endpoints.signIn.matchFulfilled, (state, { payload }) => {
            state.tkn = payload.tkn;
        });
        builder.addMatcher(authApi.endpoints.signUp.matchFulfilled, (state, { payload }) => {
            state.tkn = payload.tkn;
        });
        builder.addMatcher(authApi.endpoints.getMe.matchFulfilled, (state, { payload }) => {
            state.user = payload;
        });
        builder.addMatcher(tokenApi.endpoints.refresh.matchFulfilled, (state, { payload }) => {
            state.tkn = payload.tkn;
        });
    },
});

export const { setToken, loggedOut } = authSlice.actions;

export default authSlice.reducer;

export const selectCurrentUser = (state: RootState) => state.auth.user;

export const selectIsAuth = (state: RootState) => !!state.auth.tkn;
export const selectToken = (state: RootState) => state.auth.tkn;

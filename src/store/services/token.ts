import { TokenResponse } from './auth';
import { createApi } from '@reduxjs/toolkit/query/react';
import { refreshQuery } from 'shared/fetch';

export const tokenApi = createApi({
    baseQuery: refreshQuery,
    reducerPath: 'tokenApi',
    endpoints: (build) => ({
        refresh: build.query<TokenResponse, void>({ query: () => 'v1/auth/token' }),
    }),
});

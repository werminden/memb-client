import {
    QuizType,
    GameSession,
    PagingResult,
    PagingQuery,
    IdParamPayload,
    QuizStatus,
    QuizState,
} from 'shared/types';
import { baseApi } from './base';

export type CreateSessionPayload = {
    types: QuizType[];
    note: string;
};

type SessionStatePayload = PagingQuery & {
    gameSessionId: number;
    status?: QuizStatus;
};

export const sessionApi = baseApi.injectEndpoints({
    endpoints: (build) => ({
        getSessions: build.query<PagingResult<GameSession>, PagingQuery | undefined>({
            query: (params) => ({ url: 'v1/session', params }),
            providesTags: (result) =>
                result?.data
                    ? [
                          ...result.data.map(({ id }) => ({ type: 'SESSION' as const, id })),
                          { type: 'SESSION' },
                      ]
                    : [{ type: 'SESSION' }],
        }),
        getSessionState: build.query<PagingResult<QuizState>, SessionStatePayload>({
            query: ({ gameSessionId, ...queryParams }) => ({
                url: `v1/session/${gameSessionId}/state`,
                params: queryParams,
            }),
        }),
        createSession: build.mutation<GameSession, CreateSessionPayload>({
            query: (payload) => ({ url: 'v1/session', body: payload, method: 'POST' }),
            invalidatesTags: ['SESSION'],
        }),
        deleteSession: build.mutation<void, IdParamPayload>({
            query: ({ id }) => ({ url: `v1/session/${id}`, method: 'DELETE' }),
            invalidatesTags: ['SESSION'],
        }),
    }),
});

export const {
    useCreateSessionMutation,
    useDeleteSessionMutation,
    useLazyGetSessionStateQuery,
    useGetSessionsQuery,
    useGetSessionStateQuery,
} = sessionApi;

import { loggedOut } from 'store/slices/auth';
import { User } from 'shared/types';
import { baseApi } from './base';

export interface SignInPayload {
    user: string;
    password: string;
}

export interface SignUpPayload {
    username: string;
    email: string;
    password: string;
}

export interface TokenResponse {
    tkn: string;
}

export const authApi = baseApi.injectEndpoints({
    endpoints: (build) => ({
        getMe: build.query<User, void>({
            query: () => 'v1/auth/me',
            providesTags: ['ME'],
        }),
        signIn: build.mutation<TokenResponse, SignInPayload>({
            query: (payload) => ({ url: 'v1/auth/signin', body: payload, method: 'POST' }),
        }),
        signUp: build.mutation<TokenResponse, SignUpPayload>({
            query: (payload) => ({ url: 'v1/auth/signup', body: payload, method: 'POST' }),
        }),
        signOut: build.mutation<void, void>({
            query: () => ({
                url: 'v1/auth/signout',
                method: 'POST',
                credentials: 'include',
            }),
            async onQueryStarted(_, { queryFulfilled, dispatch }) {
                await queryFulfilled;
                dispatch(loggedOut());
                dispatch(authApi.util.resetApiState());
            },
        }),
    }),
});

export const { useSignInMutation, useSignUpMutation, useSignOutMutation, useGetMeQuery } = authApi;

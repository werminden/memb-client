import { createApi } from '@reduxjs/toolkit/query/react';
import baseQueryWithReauth from '../../shared/fetch';

export const baseApi = createApi({
    baseQuery: baseQueryWithReauth,
    reducerPath: 'api',
    tagTypes: ['ME', 'SESSION'],
    endpoints: () => ({}),
});

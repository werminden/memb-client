import { PagingQuery } from './../../shared/types/index';
import {
    Cursor,
    MessageData,
    PagingResult,
    QuizState,
    ResponseStatus,
    SocketResponse,
} from 'shared/types';
import { getSocket } from 'shared/socket';
import { baseApi } from './base';
import { removeCurrentQuizState, setCurrentQuizState, setMessages, setQuizState } from '../slices';

export enum GameEvent {
    SendMessage = 'SEND_MESSAGE',
    RequestSessionMessages = 'REQUEST_SESSION_MESSAGES',
    RequestSessionState = 'REQUEST_SESSION_STATE',
    SendSessionState = 'SEND_SESSION_STATE',
    JoinSession = 'JOIN_SESSION',
    ReceiveMessages = 'RECEIVE_MESSAGES',
    QuizStart = 'QUIZ_START',
    NextQuiz = 'NEXT_QUIZ',
    CheckResult = 'CHECK_RESULT',
    ReadMessages = 'READ_MESSAGES',
}

type GameSessionIdPayload = {
    gameSessionId: number;
};

type CheckResultPayload = GameSessionIdPayload & {
    answer: string;
};
type GetMessagesPayload = PagingQuery & GameSessionIdPayload;

type MarkAsReadPayload = GameSessionIdPayload & {
    messageIds: number[];
};

export const gameApi = baseApi.injectEndpoints({
    endpoints: (builder) => ({
        checkResult: builder.mutation<void, CheckResultPayload>({
            queryFn: async (payload) => {
                const socket = await getSocket();
                return new Promise((resolve, reject) => {
                    socket.emit(GameEvent.CheckResult, payload, (res: SocketResponse<void>) => {
                        if (res.status === ResponseStatus.NOK) {
                            return reject(res.error);
                        }
                        resolve({ data: res.data });
                    });
                });
            },
        }),
        markAsRead: builder.mutation<void, MarkAsReadPayload>({
            queryFn: async (payload) => {
                const socket = await getSocket();
                return new Promise((_, reject) => {
                    socket.emit(GameEvent.ReadMessages, payload, (res: SocketResponse<void>) => {
                        if (res.status === ResponseStatus.NOK) {
                            return reject(res.error);
                        }
                    });
                });
            },
        }),
        nextQuiz: builder.mutation<void, number>({
            queryFn: async (sessionId) => {
                const socket = await getSocket();
                return new Promise((res) => {
                    socket.emit(GameEvent.NextQuiz, sessionId, res);
                });
            },
        }),
        startQuiz: builder.mutation<void, number>({
            queryFn: async (sessionId) => {
                const socket = await getSocket();
                return new Promise((res) => {
                    socket.emit(GameEvent.QuizStart, sessionId, res);
                });
            },
        }),
        subscribeOnCurrentQuizState: builder.query<QuizState | null, number>({
            queryFn: async (sessionId) => {
                const socket = await getSocket();
                return new Promise((resolve, reject) => {
                    socket.emit(
                        GameEvent.RequestSessionState,
                        sessionId,
                        (res: SocketResponse<QuizState | null>) => {
                            if (res.status === ResponseStatus.NOK) {
                                return reject(res.error);
                            }
                            resolve({ data: res.data! });
                        },
                    );
                });
            },
            async onCacheEntryAdded(sessionId, { cacheDataLoaded, updateCachedData, dispatch }) {
                try {
                    await cacheDataLoaded;
                    const socket = await getSocket();

                    socket.on(GameEvent.SendSessionState, (data: QuizState) => {
                        console.log(data, sessionId);
                        if (!data) {
                            dispatch(removeCurrentQuizState(sessionId));
                        } else {
                            dispatch(setCurrentQuizState(data));
                            dispatch(setQuizState(data));
                        }
                        updateCachedData(() => {
                            return data;
                        });
                    });
                } catch (e) {
                    console.error(e);
                    // if cacheEntryRemoved resolved before cacheDataLoaded,
                    // cacheDataLoaded throws
                }
            },
        }),
        getMessages: builder.query<PagingResult<MessageData>, GetMessagesPayload>({
            query: ({ gameSessionId, ...queryParams }) => ({
                url: `v1/session/${gameSessionId}/messages`,
                params: queryParams,
            }),
        }),
        joinSession: builder.mutation<void, number>({
            queryFn: async (sessionId: number) => {
                const socket = await getSocket();
                return new Promise((resolve, reject) => {
                    const join = () =>
                        socket.emit(
                            GameEvent.JoinSession,
                            sessionId,
                            (res: SocketResponse<void>) => {
                                if (res.status === ResponseStatus.NOK) {
                                    return reject(res.error);
                                }
                                resolve({ data: res.data });
                            },
                        );

                    if (socket.disconnected) {
                        socket.on('connect', () => {
                            join();
                        });
                    } else {
                        join();
                    }
                });
            },
            async onCacheEntryAdded(_, { cacheDataLoaded, dispatch }) {
                try {
                    await cacheDataLoaded;
                    const socket = await getSocket();

                    socket.on(GameEvent.ReceiveMessages, (data: MessageData[]) => {
                        dispatch(setMessages(data));
                    });
                } catch (e) {
                    console.error(e);
                    // if cacheEntryRemoved resolved before cacheDataLoaded,
                    // cacheDataLoaded throws
                }
            },
        }),
        leaveSession: builder.mutation<void, number>({
            queryFn: async (sessionId: number) => {
                const socket = await getSocket();
                return new Promise((resolve) => {
                    socket.off('connect');
                    socket.off(GameEvent.SendSessionState);
                    socket.off(GameEvent.SendMessage);
                    resolve({ data: undefined });
                });
            },
        }),
    }),
});

export const {
    useJoinSessionMutation,
    useLeaveSessionMutation,
    useCheckResultMutation,
    useLazySubscribeOnCurrentQuizStateQuery,
    useNextQuizMutation,
    useStartQuizMutation,
    useGetMessagesQuery,
    useLazyGetMessagesQuery,
    useMarkAsReadMutation,
} = gameApi;

import { configureStore, combineReducers } from '@reduxjs/toolkit';
import {
    persistReducer,
    persistStore,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { authSlice } from 'store/slices/auth';
import { baseApi, tokenApi } from 'store/services';
import { quizStateSlice, sessionSlice, currentQuizStateSlice, messagesSlice } from './slices';

const authPersistConfig = {
    key: 'auth',
    storage,
};

const reducer = combineReducers({
    [tokenApi.reducerPath]: tokenApi.reducer,
    [baseApi.reducerPath]: baseApi.reducer,
    auth: persistReducer(authPersistConfig, authSlice.reducer),
    messages: messagesSlice.reducer,
    sessions: sessionSlice.reducer,
    quizState: quizStateSlice.reducer,
    currentQuizState: currentQuizStateSlice.reducer,
});

export const store = configureStore({
    reducer,
    middleware: (gDM) =>
        gDM({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }).concat(baseApi.middleware, tokenApi.middleware),
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

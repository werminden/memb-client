import { FunctionComponent, h } from 'preact';
import { useContext } from 'preact/hooks';
import { Controller, useForm } from 'react-hook-form';
import { BaseBtn, SelectField, TextField } from 'shared/ui';
import { QuizType } from 'shared/types';
import { GameContext } from 'shared/context';
import { CreateSessionPayload } from 'store/services';

type Props = {
    onSuccess?: () => void;
};

const SessionForm: FunctionComponent<Props> = ({ onSuccess }) => {
    const { addSession } = useContext(GameContext);
    const {
        register,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
        control,
        reset,
    } = useForm<CreateSessionPayload>({
        mode: 'onChange',
    });

    const onSubmit = handleSubmit(async (data) => {
        if (!isValid) return;
        await addSession(data);
        reset();
        onSuccess && onSuccess();
    });

    return (
        <form onSubmit={onSubmit as any} className="w-screen sm:w-[420px] p-4 text-gray-400">
            <div className="text-center w-full">
                <div className="flex flex-row flex-1">
                    <div className="flex flex-col flex-grow">
                        <div>
                            <h3 className="font-black text-slate-200 mb-2">Add new session</h3>
                            <p className="mb-4 font-semibold">
                                just pick needed categories and add a note
                            </p>
                        </div>
                        <div className="text-left w-full">
                            <div className="mb-5">
                                <TextField
                                    id="note"
                                    {...register('note', {
                                        minLength: 3,
                                        maxLength: 64,
                                        required: true,
                                    })}
                                    autocomplete="off"
                                    placeholder="type a label or note..."
                                />
                            </div>
                            <div className="mb-2">
                                <Controller
                                    control={control}
                                    defaultValue={[]}
                                    name="types"
                                    rules={{ required: true, minLength: 1 }}
                                    render={({ field }) => (
                                        <SelectField
                                            {...field}
                                            placeholder="type any preferable quiz type"
                                            options={Object.values(QuizType)}
                                        />
                                    )}
                                />
                            </div>
                            <BaseBtn
                                className="w-full h-10 text-black"
                                type="submit"
                                disabled={isSubmitting || !isValid}
                            >
                                Submit
                            </BaseBtn>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
};

export default SessionForm;

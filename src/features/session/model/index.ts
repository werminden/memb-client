import { useAppSelector } from 'app/hooks/auth';
import {
    CreateSessionPayload,
    sessionApi,
    useCreateSessionMutation,
    useDeleteSessionMutation,
} from 'store/services';
import { sessionCursorSelector, sessionSelector } from 'store/slices';
import { IdParamPayload } from 'shared/types';
import { useParams, useNavigate } from 'react-router-dom';
import { routes } from 'shared/constants';

export const useGameSession = () => {
    const params = useParams();
    const navigate = useNavigate();
    const [createSession, { isLoading: isCreatingSession }] = useCreateSessionMutation();
    const [deleteSession, { isLoading: isDeletingSession }] = useDeleteSessionMutation();
    const [
        fetchSessions,
        { isFetching: isFetchingSessions, isUninitialized: isUninitializedSessions },
    ] = sessionApi.endpoints.getSessions.useLazyQuery();

    const sessions = useAppSelector(sessionSelector.selectAll);
    const loadedCount = useAppSelector(sessionSelector.selectTotal);
    const cursor = useAppSelector(sessionCursorSelector);

    const selectedSessionId = typeof params?.sid !== 'undefined' ? Number(params.sid) : null;

    const addSession = async (payload: CreateSessionPayload) => {
        const data = await createSession(payload).unwrap();
        console.log(data);
        selectSession(data.id);
    };

    const removeSession = async (payload: IdParamPayload) => {
        const data = await deleteSession(payload);

        navigate(routes.game, { replace: true });
    };

    const selectSession = (sessionId: number) => {
        navigate(`${routes.game}/${sessionId}`, { replace: true });
    };

    return {
        fetchSessions,
        isFetchingSessions,
        isCreatingSession,
        isDeletingSession,
        isUninitializedSessions,
        addSession,
        selectSession,
        removeSession,
        sessions,
        loadedCount,
        cursor,
        selectedSessionId,
    };
};

import { FunctionComponent, h } from 'preact';
import { useCallback, useContext, useEffect, useMemo, useRef } from 'preact/hooks';
import InfiniteScroll from 'react-infinite-scroller';
import cn from 'classnames';
import { GameContext } from 'shared/context';
import QuizListItem from './quizListItem';
import styles from './styles.module.css';

const QuizList: FunctionComponent = () => {
    const {
        fetchSessions,
        sessions,
        cursor,
        selectSession,
        selectedSessionId,
        removeSession,
        isFetchingSessions,
    } = useContext(GameContext);
    const div = useRef(null);

    useEffect(() => {
        fetchSessions(undefined, true);
    }, []);

    const isActiveLink = useCallback(
        (sessionId: number) => {
            if (selectedSessionId === null) return false;
            return sessionId === selectedSessionId;
        },
        [selectedSessionId],
    );

    const links = useMemo(
        () =>
            sessions.map((session) => {
                return (
                    <QuizListItem
                        data={session}
                        key={`${session.id}-session-list-item`}
                        active={isActiveLink(session.id)}
                        onSelect={(data) => selectSession(data.id)}
                        onRemove={(data) => removeSession(data)}
                    />
                );
            }),
        [sessions, isActiveLink],
    );

    return (
        <div className="w-full h-full relative">
            <div className={cn(styles.wrapper, 'scrollBox')} ref={div}>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={() => {
                        if (!isFetchingSessions && cursor.afterCursor) {
                            fetchSessions({ afterCursor: cursor.afterCursor }, true);
                        }
                    }}
                    threshold={50}
                    hasMore={!!cursor.afterCursor}
                    loader={
                        <div className="loader" key={0}>
                            Loading ...
                        </div>
                    }
                    useWindow={false}
                    getScrollParent={() => div.current}
                >
                    {links}
                </InfiniteScroll>
            </div>
        </div>
    );
};

export default QuizList;

import { FunctionComponent, h } from 'preact';
import cn from 'classnames';
import { BaseBtn, Menu } from 'shared/ui';
import { GameSession } from 'shared/types';
import { FiInbox } from 'react-icons/fi';
import { useActivator } from 'shared/lib/hooks';
import { useState } from 'preact/hooks';

type SessionEvent = (data: GameSession) => void;

type Props = {
    data: GameSession;
    active?: boolean;
    onSelect?: SessionEvent;
    onRemove?: SessionEvent;
};

const QuizListItem: FunctionComponent<Props> = ({ data, active, onSelect, onRemove }) => {
    const { open, anchorEl, isOpen, close } = useActivator();
    const [focused, setFocused] = useState(false);

    return (
        <div>
            <BaseBtn
                className={cn(
                    'w-full !font-black px-4 hover:!text-slate-200',
                    active ? '!text-slate-200 bg-btnlight' : '!text-slate-400',
                )}
                onMouseEnter={() => setFocused(true)}
                onMouseLeave={() => setFocused(false)}
                onClick={() => onSelect && onSelect(data)}
            >
                <div className="flex flex-row items-center justify-between">
                    <div>
                        <FiInbox className="inline mr-2" size={20} />
                        {data.note}
                    </div>
                    <a
                        className={cn(
                            'z-10 transition-all duration-100 delay-50 opacity-0 pointer-events-none',
                            {
                                'opacity-100 pointer-events-auto': focused || isOpen,
                            },
                        )}
                        onMouseDown={(e) => e.stopPropagation()}
                        onClick={(e) => {
                            e.stopPropagation();
                            open(e);
                        }}
                    >
                        x
                    </a>
                </div>
            </BaseBtn>
            <Menu className="bg-slate-700" onClose={close} anchorEl={anchorEl} isOpen={isOpen}>
                <div className="w-screen sm:w-[300px] p-2 text-gray-400">
                    <div className="text-center w-full">
                        <div>
                            <h3 className="font-black text-slate-200 mb-2">Remove session</h3>
                            <p className="mb-4 font-semibold">Are you sure with this action?</p>
                        </div>
                    </div>
                    <div className="flex flex-row justify-center">
                        <BaseBtn
                            onClick={() => {
                                onRemove && onRemove(data);
                            }}
                        >
                            Agree
                        </BaseBtn>
                        <BaseBtn onClick={close}>Cancel</BaseBtn>
                    </div>
                </div>
            </Menu>
        </div>
    );
};

export default QuizListItem;

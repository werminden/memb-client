import { createRef, FunctionComponent, h, JSX } from 'preact';
import InfiniteScroll from 'react-infinite-scroller';
import { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'preact/hooks';
import { BaseBtn } from 'shared/ui';
import { QuizStatus } from 'shared/types';
import GameInput from './ui/gameInput';
import Message from './ui/message';
import { GameContext } from 'shared/context';
import {
    useJoinSessionMutation,
    useLeaveSessionMutation,
    useLazySubscribeOnCurrentQuizStateQuery,
    useLazyGetMessagesQuery,
    useNextQuizMutation,
    useStartQuizMutation,
    useCheckResultMutation,
    useGetSessionStateQuery,
    useMarkAsReadMutation,
} from 'store/services';
import {
    currentQuizStatesAdapter,
    messagesCursorSelector,
    messagesSelectorBySessionId,
    quizStatesEntityByQuizForSessionSelector,
} from 'store/slices';
import { useAppSelector } from 'app/hooks/auth';
import { useDebounce } from 'shared/lib/hooks/debounce';

const ChatBox: FunctionComponent = () => {
    const { selectedSessionId } = useContext(GameContext);
    const debounce = useDebounce({ delay: 300 });
    const [seenIds, setSeenIds] = useState<number[]>([]);
    const [joinSession] = useJoinSessionMutation();
    const [leaveSession] = useLeaveSessionMutation();
    const [markAsRead] = useMarkAsReadMutation();
    const [subscribeOnCurrentQuizStateQuery] = useLazySubscribeOnCurrentQuizStateQuery();
    const [fetchMessages, { isFetching: isMessagesFetching }] = useLazyGetMessagesQuery();
    useGetSessionStateQuery(
        { gameSessionId: selectedSessionId!, status: QuizStatus.done },
        { skip: !selectedSessionId },
    );
    const [nextQuiz] = useNextQuizMutation();
    const [startQuiz] = useStartQuizMutation();
    const [checkResult] = useCheckResultMutation();

    const currentQuizState = useAppSelector((state) =>
        currentQuizStatesAdapter
            .getSelectors()
            .selectById(state.currentQuizState, selectedSessionId!),
    );
    const messages = useAppSelector((state) =>
        messagesSelectorBySessionId(state, selectedSessionId!),
    );
    const messagesCursor = useAppSelector(messagesCursorSelector);
    const quizStatesEntityByQuiz = useAppSelector((state) =>
        quizStatesEntityByQuizForSessionSelector(state, selectedSessionId!),
    );

    const mainBox = useRef<HTMLDivElement>(null);

    const btnRef = createRef<HTMLButtonElement>();

    useEffect(() => {
        debounce(async () => {
            if (seenIds.length) {
                await markAsRead({
                    gameSessionId: selectedSessionId!,
                    messageIds: seenIds,
                }).unwrap();
                setSeenIds([]);
            }
        });
    }, [seenIds, selectedSessionId]);

    useEffect(() => {
        if (selectedSessionId) {
            (async () => {
                await joinSession(selectedSessionId).unwrap();

                subscribeOnCurrentQuizStateQuery(selectedSessionId);
                fetchMessages({ gameSessionId: selectedSessionId, limit: 30 }, true);
                scrollToEnd();
            })();
        }
        return () => {
            leaveSession(selectedSessionId!);
            setSeenIds([]);
        };
    }, [selectedSessionId]);

    const sendMessage = useCallback(
        async (value: string) => {
            await checkResult({ gameSessionId: selectedSessionId!, answer: value }).unwrap();
            scrollToEnd();
        },
        [checkResult, selectedSessionId],
    );

    const scrollToEnd = () => {
        setTimeout(() => mainBox.current?.scrollTo({ top: mainBox.current.scrollHeight }), 100);
    };

    const handleKeyDown: JSX.KeyboardEventHandler<HTMLDivElement> = (event) => {
        const pressKey = event.keyCode ? event.keyCode : event.which;
        if (pressKey === 13) {
            if (btnRef.current && !btnRef.current.disabled) {
                btnRef.current.click();
            }
        }
    };

    const renderMessages = useMemo(
        () =>
            messages.map((message) => (
                <Message
                    key={`message_${message.id}`}
                    data={message}
                    state={quizStatesEntityByQuiz[message.quizId]?.result}
                    contextMenu={[{ text: 'filter by quiz', cb: () => console.log('hello') }]}
                    onSeen={(id) => setSeenIds((prevState) => [...prevState, id])}
                />
            )),
        [messages, quizStatesEntityByQuiz],
    );

    const btnText = !currentQuizState
        ? 'start'
        : currentQuizState?.status === QuizStatus.story
        ? 'got it'
        : 'repeat';

    const btnAction = useMemo(
        () =>
            currentQuizState?.status === QuizStatus.story
                ? () => startQuiz(selectedSessionId!)
                : () => nextQuiz(selectedSessionId!),
        [currentQuizState, selectedSessionId, startQuiz, nextQuiz],
    );

    const renderBtn = useMemo(
        () =>
            currentQuizState?.status === QuizStatus.question ? (
                <GameInput quiz={currentQuizState.quiz} onChange={sendMessage} />
            ) : (
                <div className="flex justify-center">
                    <BaseBtn
                        className="w-full bg-btnlight"
                        onClick={async () => {
                            await btnAction().unwrap();
                            scrollToEnd();
                        }}
                        ref={btnRef}
                    >
                        {btnText}
                    </BaseBtn>
                </div>
            ),
        [currentQuizState, btnRef, btnText, btnAction, sendMessage],
    );

    if (!selectedSessionId) {
        return null;
    }

    return (
        <div className="flex flex-col h-full px-4">
            <div className="text-cyan-700 mb-4">{`#`}</div>
            <div
                className="w-full h-full flex flex-col rounded-md outline-none overflow-y-auto scroll-smooth"
                tabIndex={0}
                ref={mainBox}
                onKeyDown={handleKeyDown}
            >
                <div className="text-cyan-200 flex-grow">
                    <InfiniteScroll
                        pageStart={0}
                        isReverse
                        loadMore={() => {
                            if (!isMessagesFetching && messagesCursor.afterCursor) {
                                fetchMessages(
                                    {
                                        gameSessionId: selectedSessionId,
                                        afterCursor: messagesCursor.afterCursor,
                                    },
                                    true,
                                );
                            }
                        }}
                        hasMore={!!messagesCursor.afterCursor}
                        loader={
                            <div className="loader" key={0}>
                                Loading ...
                            </div>
                        }
                        useWindow={false}
                        getScrollParent={() => mainBox.current}
                    >
                        {renderMessages}
                    </InfiniteScroll>
                </div>
                {/* {quizMessageFilter !== undefined && (
                <div className="flex justify-center">
                <BaseBtn
                className="w-full"
                onClick={() => {
                    setQuizMessageFilter(undefined);
                }}
                disabled={isBusy}
                ref={beginBtn}
                >
                cancel
                </BaseBtn>
                </div>
            )} */}
                {renderBtn}
            </div>
        </div>
    );
};

export default ChatBox;

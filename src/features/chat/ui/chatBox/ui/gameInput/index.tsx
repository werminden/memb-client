import { FunctionComponent, h, JSX } from 'preact';
import { BaseBtn } from 'shared/ui';
import { Quiz, QuizType } from 'shared/types';
import { useContext, useEffect, useRef } from 'preact/hooks';
import { FocusContext } from 'shared/context';
import VariantInput from '../variantInput';

type Props = {
    quiz: Quiz;
    disabled?: boolean;
    onChange: (value: string) => void;
};

const GameInput: FunctionComponent<Props> = ({ quiz, disabled, onChange }) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const { currentFocus } = useContext(FocusContext);

    useEffect(() => {
        inputRef.current?.focus();

        return () => (currentFocus as HTMLInputElement).focus();
    }, []);

    const onSubmit: JSX.GenericEventHandler<HTMLFormElement> = (event) => {
        event.preventDefault();
        if (disabled) return;
        onChange((event.target as HTMLFormElement).answer.value);
        event.currentTarget.reset();
    };

    return quiz.type === QuizType.text ? (
        <form onSubmit={onSubmit} className="flex">
            <input ref={inputRef} className="flex-1" name="answer" />
            <BaseBtn className="w-[20%]" type="submit" disableSoftly={disabled}>
                send
            </BaseBtn>
        </form>
    ) : (
        <VariantInput items={quiz.variants} onChange={onChange} disabled={disabled} />
    );
};

export default GameInput;

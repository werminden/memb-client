import { FunctionComponent, h, JSX } from 'preact';
import { BaseBtn } from 'shared/ui';
import { useContext, useEffect, useMemo, useRef, useState } from 'preact/hooks';
import { WindowSizeContext } from 'shared/context';
import { SelectItem } from 'shared/types';

type Props = {
    items?: (SelectItem | string)[];
    disabled?: boolean;
    onChange: (value: string) => void;
};

const VariantInput: FunctionComponent<Props> = ({ items = [], disabled, onChange }) => {
    const inputRef = useRef<HTMLDivElement>(null);
    const { isMobile } = useContext(WindowSizeContext);
    const [focused, setFocused] = useState<number>(0);

    useEffect(() => {
        (inputRef.current?.children.item(focused) as HTMLButtonElement).focus();
    }, [focused]);

    const variants = useMemo(
        () => items.map((item) => (typeof item === 'string' ? { value: item, text: item } : item)),
        [items],
    );

    const focusNext = () => {
        setFocused((val) => (val + 1 >= variants.length ? 0 : val + 1));
    };
    const focusPrev = () => {
        setFocused((val) => (val - 1 < 0 ? variants.length - 1 : val - 1));
    };

    const handleKeyDown: JSX.KeyboardEventHandler<HTMLDivElement> = (event) => {
        if (event.key === 'ArrowRight') {
            focusNext();
        }
        if (event.key === 'ArrowLeft') {
            focusPrev();
        }
        event.stopPropagation();
    };

    const handleMouseEnter = (variantIndex: number) => {
        setFocused(variantIndex);
    };

    return (
        <div
            className="flex justify-center outline-none"
            tabIndex={0}
            ref={inputRef}
            onKeyDown={handleKeyDown}
        >
            {variants.map((variant, index) => {
                return (
                    <BaseBtn
                        key={`variant-${index}`}
                        tabIndex={0}
                        showFocus={!isMobile}
                        className="w-full outline-none"
                        onClick={() => onChange(variant.value)}
                        onMouseEnter={() => handleMouseEnter(index)}
                        disableSoftly={disabled}
                    >
                        {variant.text}
                    </BaseBtn>
                );
            })}
        </div>
    );
};

export default VariantInput;

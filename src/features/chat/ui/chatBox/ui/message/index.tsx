import { cloneElement, Fragment, FunctionComponent, h, JSX, VNode } from 'preact';
import { useInView } from 'react-intersection-observer';
import cn from 'classnames';
import { useEffect, useMemo, useState } from 'preact/hooks';
import { AnswerResult, MessageData } from 'shared/types';
import { Menu, MenuItem } from 'shared/ui';
import { useActivator } from 'shared/lib/hooks';
import styles from './styles.module.css';

interface ContextMenuItem {
    text?: string;
    cb?: () => void;
    component?: VNode;
}

type Props = {
    data: MessageData;
    state?: AnswerResult;
    contextMenu?: ContextMenuItem[];
    onSeen?: (messageId: number) => void;
};

const MessageStateStyles = {
    [AnswerResult.passed]: 'bg-green-300 text-slate-800',
    [AnswerResult.failed]: 'bg-red-300 text-slate-800',
    [AnswerResult.partially]: 'bg-orange-300 text-slate-800',
};

const Message: FunctionComponent<Props> = ({
    data: {
        sender: { username },
        text,
        params,
        read,
        id,
        createdAt,
    },
    state,
    children,
    contextMenu,
    onSeen,
}) => {
    const [isClicked, setIsClicked] = useState(false);
    const { open, anchorEl, isOpen, clickPosition, close } = useActivator();

    const { ref, inView } = useInView({ threshold: 1, triggerOnce: true });

    useEffect(() => {
        if (!read && inView && onSeen) {
            onSeen(id);
        }
    }, [inView, read, id]);

    const messageStateClasses =
        typeof state === 'number' ? MessageStateStyles[state] : 'bg-slate-400';
    const shortName = useMemo(
        () =>
            username
                .split(' ')
                .map((word) => word.charAt(0))
                .join('')
                .toUpperCase(),
        [username],
    );

    const messageBody = useMemo(
        () =>
            text.replace(/{([\w\d]+)}/g, (_, placeholderWithoutDelimiters) => {
                return params && params[placeholderWithoutDelimiters]
                    ? `<b>${params[placeholderWithoutDelimiters]}</b>`
                    : '<b>secret</b>';
            }),
        [text, params],
    );

    const dangerouslySetInnerHTML =
        typeof messageBody === 'string'
            ? {
                  // add purify
                  __html: messageBody,
              }
            : undefined;

    const pointMessage = () => setIsClicked(true);
    const unpointMessage = () => setIsClicked(false);

    const handleMouseDown: JSX.MouseEventHandler<HTMLDivElement> = (event) => {
        if (event.target === event.currentTarget) {
            pointMessage();
        }
    };

    const handleMouseLeave: JSX.MouseEventHandler<HTMLDivElement> = () => {
        if (!isOpen) {
            unpointMessage();
        }
    };

    const handleMouseUp: JSX.MouseEventHandler<HTMLDivElement> = () => {
        if (!isOpen) {
            unpointMessage();
        }
    };

    const handleContextMenu: JSX.MouseEventHandler<HTMLDivElement> = (e) => {
        open(e);
        e.preventDefault();
    };

    const handleClose = () => {
        unpointMessage();
        close();
    };

    const contextMenuRenderer = useMemo(
        () =>
            contextMenu?.map(({ text, cb, component }) =>
                component ? (
                    cloneElement(component, { onclick: cb }, text)
                ) : (
                    <MenuItem onClick={cb}>{text}</MenuItem>
                ),
            ),
        [contextMenu],
    );

    return (
        <Fragment>
            <div
                ref={ref}
                className={cn(styles.root, {
                    [styles.active]: isClicked,
                    'bg-slate-700 my-1': !read,
                })}
                onMouseDown={handleMouseDown}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseLeave}
                onContextMenu={contextMenu ? handleContextMenu : undefined}
            >
                <div
                    className={cn(
                        'mx-2 mt-1 rounded-full h-8 w-8 text-center leading-8 items-center font-semibold pointer-events-none',
                        messageStateClasses,
                    )}
                >
                    {shortName}
                </div>
                <div className="flex-grow pointer-events-none">
                    <span className="text-sm font-thin">{username}</span>
                    <div
                        // add purify
                        dangerouslySetInnerHTML={dangerouslySetInnerHTML}
                        className="max-w-[300px] text-sm break-words whitespace-normal"
                    >
                        {children}
                    </div>
                </div>
                <div className="text-xs text-right mr-4 pointer-events-none font-thin">
                    {new Date(createdAt).toLocaleString()}
                </div>
            </div>
            {contextMenu && (
                <Menu
                    position={clickPosition}
                    onClose={handleClose}
                    anchorEl={anchorEl}
                    isOpen={isOpen}
                    onClick={handleClose}
                >
                    {contextMenuRenderer}
                </Menu>
            )}
        </Fragment>
    );
};

export default Message;

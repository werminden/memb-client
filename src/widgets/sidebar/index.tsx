import { FunctionalComponent, h } from 'preact';
import { FiHome, FiTool } from 'react-icons/fi';
import SidebarIcon from './icon';
import { Link } from 'react-router-dom';
import { routes } from 'shared/constants';
import styles from './styles.module.css';

const Sidebar: FunctionalComponent = () => {
    return (
        <div className={styles.root}>
            <nav className="my-4 max-w-xs mx-auto">
                <Link to={routes.game}>
                    <SidebarIcon tooltip="profile">VB</SidebarIcon>
                </Link>
                <div className="shadow-inner shadow-cyan-100 w-[70%] h-1 m-auto" />
                <Link to="/">
                    <SidebarIcon tooltip="home">
                        <FiHome />
                    </SidebarIcon>
                </Link>
                <Link to={routes.settings}>
                    <SidebarIcon tooltip="settings">
                        <FiTool />
                    </SidebarIcon>
                </Link>
            </nav>
        </div>
    );
};

export default Sidebar;

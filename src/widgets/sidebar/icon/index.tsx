import { FunctionalComponent, h } from 'preact';

type Props = { tooltip: string };

const SidebarIcon: FunctionalComponent<Props> = ({ children, tooltip }) => {
    return (
        <div className="sidebar-icon glow-text glow-border group">
            {children}
            <div className="sidebar-tooltip group-hover:opacity-100">{tooltip}</div>
        </div>
    );
};

export default SidebarIcon;

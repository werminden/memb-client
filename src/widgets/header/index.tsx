import { Fragment, FunctionalComponent, h } from 'preact';
import { useActivator } from 'shared/lib/hooks/activator';
import { MenuItem, Menu, NavLink, NavLinkBtn } from 'shared/ui';
import { useAppSelector } from 'app/hooks/auth';
import { selectIsAuth } from 'store/slices/auth';
import { useSignOutMutation } from '../../store/services/auth';
import { Link } from 'react-router-dom';
import { routes } from 'shared/constants';
import styles from './styles.module.css';

const Header: FunctionalComponent = () => {
    const isAuth = useAppSelector(selectIsAuth);
    const [signOut] = useSignOutMutation();
    const { open, anchorEl, isOpen, close } = useActivator();

    return (
        <header className={styles.root}>
            <div className="flex justify-between h-full w-full items-center font-bold text-lg">
                <NavLink to="/">MEM</NavLink>
                <nav className="flex flex-1 mx-32 justify-start">
                    <NavLink to={'/about'} className="mx-3">
                        About
                    </NavLink>
                    <NavLink to={routes.game} className="mx-3">
                        Game
                    </NavLink>
                    <NavLink to="/blog" className="mx-3">
                        Blog
                    </NavLink>
                </nav>

                {isAuth ? (
                    <Fragment>
                        <NavLinkBtn to="#" onClick={open as any}>
                            VB
                        </NavLinkBtn>
                        <Menu
                            className="bg-stone-800"
                            onClose={close}
                            anchorEl={anchorEl}
                            isOpen={isOpen}
                            onClick={close}
                        >
                            <MenuItem component={<Link to={routes.settings} />}>settings</MenuItem>
                            <hr className="border-cyan-300 my-1" />
                            <MenuItem onClick={() => signOut()}>logout</MenuItem>
                        </Menu>
                    </Fragment>
                ) : (
                    <NavLinkBtn to={routes.login}>login</NavLinkBtn>
                )}
            </div>
        </header>
    );
};

export default Header;

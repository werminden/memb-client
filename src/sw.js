import { getFiles, setupPrecaching, setupRouting } from 'preact-cli/sw/';
import { registerRoute } from 'workbox-routing';
import { NetworkFirst } from 'workbox-strategies';

registerRoute(({ url }) => url.pathname.startsWith('/api/'), new NetworkFirst());
setupRouting();
setupPrecaching(getFiles());

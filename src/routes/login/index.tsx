import { FunctionalComponent, h } from 'preact';
import LoginForm from './form';

const LoginPage: FunctionalComponent = () => {
    return (
        <div className="relative min-h-screen w-screen overflow-auto">
            <div className="absolute top-0 left-0 min-h-[580px] flex justify-center items-center w-full h-full">
                <LoginForm />
            </div>
        </div>
    );
};

export default LoginPage;

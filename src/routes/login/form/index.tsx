import { FunctionComponent, h } from 'preact';
import { useForm } from 'react-hook-form';
import { To, useLocation, useNavigate } from 'react-router-dom';
import { BaseBtn, NavLink, TextField } from 'shared/ui';

import { SignInPayload, useSignInMutation } from 'store/services';
import { routes } from 'shared/constants';

type LocationState = {
    from?: To;
};

const LoginForm: FunctionComponent = () => {
    const { register, handleSubmit, formState } = useForm<SignInPayload>();
    const [signIn] = useSignInMutation();
    const location = useLocation();
    const navigate = useNavigate();

    const onSubmit = handleSubmit(async (data) => {
        await signIn(data).unwrap();
        navigate((location.state as LocationState).from || '/', { replace: true });
    });

    return (
        <form
            onSubmit={onSubmit as any}
            className="w-screen sm:w-[600px] md:w-[784px] p-8 bg-slate-800 text-gray-400"
        >
            <div className="text-center w-full">
                <div className="flex flex-start flex-row flex-1">
                    <div className="flex flex-col flex-grow">
                        <div>
                            <h2 className="font-black uppercase text-slate-300 text-xl">
                                Welcome back!
                            </h2>
                            <div className="font-semibold">and what about quizzes?..</div>
                        </div>
                        <div className="text-left w-full mt-4">
                            <div className="mb-5">
                                <div className="mb-2">
                                    <label className="font-bold uppercase" htmlFor="login">
                                        email or username
                                    </label>
                                </div>
                                <TextField id="login" {...register('user')} />
                            </div>
                            <div className="mb-2">
                                <div className="mb-2">
                                    <label className="font-bold uppercase" htmlFor="password">
                                        password
                                    </label>
                                </div>
                                <TextField
                                    id="password"
                                    type="password"
                                    {...register('password')}
                                />
                            </div>
                            <NavLink className="block mb-5 font-semibold" to="#">
                                forgot password?
                            </NavLink>
                            <BaseBtn
                                className="w-full mb-4 h-14 text-xl border-2 border-slate-300"
                                type="submit"
                            >
                                Login
                            </BaseBtn>
                            <div className="font-bold">
                                <span className="opacity-60">Need an account? </span>
                                <NavLink to={`/${routes.register}`}>Register</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
};

export default LoginForm;

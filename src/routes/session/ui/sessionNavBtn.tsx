import { FunctionComponent, h, JSX, VNode } from 'preact';
import cn from 'classnames';
import { BaseBtn } from 'shared/ui';

type Props = {
    active?: boolean;
    component?: VNode;
};

const SessionNavBtn: FunctionComponent<Props & JSX.HTMLAttributes> = ({
    active,
    children,
    className,
    component,
    ...restProps
}) => {
    return (
        <BaseBtn
            component={component}
            className={cn(
                '!font-black px-4 hover:!text-slate-200',
                active ? '!text-slate-200 bg-btnlight' : '!text-slate-400',
                className,
            )}
            {...restProps}
        >
            {children}
        </BaseBtn>
    );
};

export default SessionNavBtn;

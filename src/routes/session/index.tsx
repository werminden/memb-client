import { FunctionalComponent, h } from 'preact';
import Sidebar from 'widgets/sidebar';
import { SessionList, SessionForm } from 'features/session';
import { GameProvider } from 'app/providers';
import { useActivator } from 'shared/lib/hooks';
import { Menu } from 'shared/ui';
import SessionNavBtn from './ui/sessionNavBtn';
import { Link, Outlet, useMatch } from 'react-router-dom';
import { routes } from '../../shared/constants';
import { FiArchive } from 'react-icons/fi';

const SessionPage: FunctionalComponent = () => {
    const { open, anchorEl, isOpen, close } = useActivator();
    const matchQuiz = useMatch(routes.quiz);

    return (
        <GameProvider>
            <div className="flex h-full">
                <Sidebar />
                <div className="px-2 py-4 bg-slate-700 w-[320px] max-h-full">
                    <SessionNavBtn
                        component={<Link to={routes.quiz} />}
                        href={routes.quiz}
                        className="w-full"
                        active={!!matchQuiz}
                    >
                        <div className="text-left items-center">
                            <FiArchive className="inline mr-2" size={18} />
                            quizzes
                        </div>
                    </SessionNavBtn>

                    <div className="flex flex-row justify-between text-slate-300 font-bold items-center p-2">
                        <div>started sessions</div>
                        <button onClick={open} className="font-black">
                            +
                        </button>
                        <Menu
                            className="bg-slate-700"
                            onClose={close}
                            anchorEl={anchorEl}
                            isOpen={isOpen}
                        >
                            <SessionForm onSuccess={close} />
                        </Menu>
                    </div>
                    <SessionList />
                </div>
                <div className="p-4 h-full w-full overflow-hidden">
                    <div className="w-full h-full">
                        <Outlet />
                    </div>
                </div>
            </div>
        </GameProvider>
    );
};

export default SessionPage;

import { FunctionalComponent, h } from 'preact';
import Sidebar from 'widgets/sidebar';

const SettingsPage: FunctionalComponent = () => {
    return (
        <div className="flex bg-sky-900 flex-col sm:flex-row">
            <Sidebar />
            <div>
                <h1>settings</h1>
            </div>
        </div>
    );
};

export default SettingsPage;

import { FunctionalComponent, h } from 'preact';
import Header from 'widgets/header';

const HomePage: FunctionalComponent = () => {
    return (
        <div className="container mx-auto">
            <Header />
            <div className="h-[3000px]">
                <h1>home</h1>
            </div>
        </div>
    );
};

export default HomePage;

import { FunctionComponent, h } from 'preact';
import { useForm } from 'react-hook-form';
import { BaseBtn, NavLink, TextField } from 'shared/ui';
import { useNavigate } from 'react-router-dom';
import { SignUpPayload, useSignUpMutation } from 'store/services';
import { routes } from 'shared/constants';

const RegisterForm: FunctionComponent = () => {
    const navigate = useNavigate();
    const [signUp] = useSignUpMutation();
    const {
        register,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm<SignUpPayload>();

    const onSubmit = handleSubmit(async (data) => {
        await signUp(data).unwrap();
        navigate('/', { replace: true });
    });

    return (
        <form
            onSubmit={onSubmit as any}
            className="w-screen sm:w-[600px] md:w-[784px] p-8 bg-slate-800 text-gray-400"
        >
            <div className="text-center w-full">
                <div className="flex flex-start flex-row flex-1">
                    <div className="flex flex-col flex-grow">
                        <div>
                            <h2 className="font-black uppercase text-slate-300 text-xl">
                                Create an account
                            </h2>
                            <div className="font-semibold">to do some quizzes, right?..</div>
                        </div>
                        <div className="text-left w-full mt-5">
                            <div className="mb-5">
                                <div className="mb-2">
                                    <label className="font-bold uppercase" htmlFor="email">
                                        email
                                    </label>
                                </div>
                                <TextField id="email" {...register('email')} />
                            </div>
                            <div className="mb-5">
                                <div className="mb-2">
                                    <label className="font-bold uppercase" htmlFor="username">
                                        username
                                    </label>
                                </div>
                                <TextField id="username" {...register('username')} />
                            </div>
                            <div className="mb-8">
                                <div className="mb-2">
                                    <label className="font-bold uppercase" htmlFor="password">
                                        password
                                    </label>
                                </div>
                                <TextField
                                    id="password"
                                    type="password"
                                    {...register('password')}
                                />
                            </div>
                            <BaseBtn
                                className="w-full mb-4 h-14 text-xl border-2 border-slate-300"
                                type="submit"
                                disabled={isSubmitting}
                            >
                                Register
                            </BaseBtn>
                            <div>
                                <NavLink to={`/${routes.login}`} className="font-semibold">
                                    Already have an account?
                                </NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
};

export default RegisterForm;

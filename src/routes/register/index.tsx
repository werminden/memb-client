import { FunctionalComponent, h } from 'preact';
import RegisterForm from './form';

const RegisterPage: FunctionalComponent = () => {
    return (
        <div className="relative min-h-screen w-screen overflow-auto">
            <div className="absolute top-0 left-0 min-h-[580px] flex justify-center items-center w-full h-full">
                <RegisterForm />
            </div>
        </div>
    );
};

export default RegisterPage;

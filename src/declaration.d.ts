import { UnpackNestedValue, FieldValues, SubmitErrorHandler } from 'react-hook-form';
import { JSX } from 'preact';

declare module '*.css' {
    const mapping: Record<string, string>;
    export default mapping;
}

declare module '*.scss' {
    const mapping: Record<string, string>;
    export default mapping;
}

declare module 'react-hook-form' {
    export declare type SubmitHandler<TFieldValues extends FieldValues> = (
        data: UnpackNestedValue<TFieldValues>,
        event?: JSX.GenericEventHandler<HTMLFormElement> | React.BaseSyntheticEvent,
    ) => any | Promise<any>;

    export declare type UseFormHandleSubmit<TFieldValues extends FieldValues> = <_>(
        onValid: SubmitHandler<TFieldValues>,
        onInvalid?: SubmitErrorHandler<TFieldValues>,
    ) => (e?: JSX.GenericEventHandler<HTMLFormElement> | React.BaseSyntheticEvent) => Promise<void>;
}

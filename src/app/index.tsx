import { FunctionalComponent, h } from 'preact';

import SessionPage from 'routes/session';
import HomePage from 'routes/home';
import SettingsPage from 'routes/settings';
import NotFoundPage from 'routes/notfound';
import LoginPage from 'routes/login';
import RegisterPage from 'routes/register';
import { useTheme } from './hooks';
import { WindowSizeProvider, PrevFocusProvider, StoreProvider } from './providers';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PrivateRoute from 'shared/components/privateRoute';
import NotPrivateRoute from 'shared/components/notPrivateRoute';
import { routes } from 'shared/constants';
import { ChatBox } from 'features/chat';
import { QuizBox } from 'features/quiz';

const App: FunctionalComponent = () => {
    useTheme();

    return (
        <div id="appMount" className="absolute bg-stone-900 w-full h-full overflow-hidden">
            <StoreProvider>
                <PrevFocusProvider>
                    <WindowSizeProvider>
                        <BrowserRouter>
                            <Routes>
                                <Route path="/">
                                    <Route index element={<HomePage />} />
                                    <Route element={<PrivateRoute />}>
                                        <Route path={routes.game} element={<SessionPage />}>
                                            <Route index />
                                            <Route path="quiz" element={<QuizBox />} />
                                            <Route path=":sid" element={<ChatBox />} />
                                        </Route>
                                        <Route path={routes.settings} element={<SettingsPage />} />
                                    </Route>
                                    <Route element={<NotPrivateRoute redirectTo={routes.game} />}>
                                        <Route path={routes.login} element={<LoginPage />} />
                                        <Route path={routes.register} element={<RegisterPage />} />
                                    </Route>
                                    <Route path="*" element={<NotFoundPage />} />
                                </Route>
                            </Routes>
                        </BrowserRouter>
                    </WindowSizeProvider>
                </PrevFocusProvider>
            </StoreProvider>
        </div>
    );
};

export default App;

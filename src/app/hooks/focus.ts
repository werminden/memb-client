import { useEffect, useState } from 'preact/hooks';

export const useFocusHistory = () => {
    const [currentFocus, setFocus] = useState<Element | null>(null);
    const [prevFocus, setPrevFocus] = useState<Element | null>(null);

    useEffect(() => {
        const saveActiveElement = () => {
            setPrevFocus(currentFocus || document.activeElement);
            setFocus(document.activeElement);
        };
        window.addEventListener('focusin', saveActiveElement);
        saveActiveElement();

        return () => window.removeEventListener('focusin', saveActiveElement);
    }, []);

    return { currentFocus, prevFocus };
};

import { useEffect, useState } from 'preact/hooks';
import { DEFAULT_WINDOW_SIZE, WindowSize } from 'shared/context';
import tailwindConfig from '../../../tailwind.config';

const mobileBreakpoint = Number(tailwindConfig.theme.screens.md.replace('px', ''));

export const useWindowSize = () => {
    const [windowSize, setWindowSize] = useState<WindowSize>(DEFAULT_WINDOW_SIZE.windowSize);

    useEffect(() => {
        function handleResize() {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        window.addEventListener('resize', handleResize);

        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return { windowSize, isMobile: (windowSize.width || 0) < mobileBreakpoint };
};

import { FunctionComponent, h } from 'preact';
import { useFocusHistory } from 'app/hooks';
import { FocusContext } from 'shared/context';

const PrevFocusProvider: FunctionComponent = ({ children }) => {
    const focusHistory = useFocusHistory();

    return <FocusContext.Provider value={focusHistory}>{children}</FocusContext.Provider>;
};

export default PrevFocusProvider;

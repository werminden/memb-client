import { FunctionComponent, h } from 'preact';
import { useWindowSize } from 'app/hooks';
import { WindowSizeContext } from 'shared/context';

const WindowSizeProvider: FunctionComponent = ({ children }) => {
    const windowSize = useWindowSize();

    return <WindowSizeContext.Provider value={windowSize}>{children}</WindowSizeContext.Provider>;
};

export default WindowSizeProvider;

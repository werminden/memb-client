import { FunctionComponent, h } from 'preact';
import { persistor, store } from 'store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { authApi } from 'store/services';

const StoreProvider: FunctionComponent = ({ children }) => {
    store.dispatch(authApi.endpoints.getMe.initiate());
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>{children}</PersistGate>
        </Provider>
    );
};

export default StoreProvider;

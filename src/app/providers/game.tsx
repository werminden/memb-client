import { FunctionComponent, h } from 'preact';
import { GameContext } from 'shared/context';
import { useGameSession } from 'features/session/model';

const GameProvider: FunctionComponent = ({ children }) => {
    const session = useGameSession();

    return <GameContext.Provider value={{ ...session }}>{children}</GameContext.Provider>;
};

export default GameProvider;
